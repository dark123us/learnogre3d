#include "ExampleApplication.h"

class Example1: public ExampleApplication{
public:
	void createScene(){
		Ogre::Entity *ent = mSceneMgr->createEntity("MyEntity", "Sinbad.mesh");
		//Ogre::Entity *ent2 = mSceneMgr->createEntity("MyEntity2", "ogrehead.mesh");
		//mSceneMgr->getRootSceneNode()->attachObject(ent);
		Ogre::SceneNode *node = mSceneMgr->createSceneNode("Node1");
		//Ogre::SceneNode *node2 = mSceneMgr->createSceneNode("Node2");
		mSceneMgr->getRootSceneNode()->addChild(node);
		node->attachObject(ent);
		//node->addChild(node2);
		//node2->setPosition(0,10,20);
		//node2->attachObject(ent2);
	}
};

class Example2: public ExampleApplication{
public:
	void createScene(){
		Ogre::Plane plane(Vector3::UNIT_Y, -10);
		Ogre::MeshManager::getSingleton().createPlane("plane", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
			plane,1500,1500,20,20,true,1,5,5,Vector3::UNIT_Z);
		Ogre::Entity *ent = mSceneMgr->createEntity("LightPlaneEntity", "plane");
		mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(ent);
		ent->setMaterialName("Examples/BeachStones");
	}
};

class Example3: public ExampleApplication{
public:
	void createScene(){
		Ogre::Plane plane(Vector3::UNIT_Y, -10);
		Ogre::MeshManager::getSingleton().createPlane("plane",
			ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane,
			1500,1500,200,200,true,1,5,5,Vector3::UNIT_Z);
		Ogre::Entity *ent = mSceneMgr->createEntity("LightPlaneEntity", "plane");
		mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(ent);
		ent->setMaterialName("Examples/BeachStones");

		Ogre::SceneNode *node = mSceneMgr->createSceneNode("Node1");
		mSceneMgr->getRootSceneNode()->addChild(node);

		Ogre::Light *light1 = mSceneMgr->createLight("Light1");
		light1->setType(Ogre::Light::LT_POINT);
		light1->setPosition(0,20,0);
		light1->setDiffuseColour(1.0f, 1.0f, 1.0f);

		Ogre::Entity *LightEnt = mSceneMgr->createEntity("MyEntity", "sphere.mesh");
		Ogre::SceneNode* node3 = node->createChildSceneNode("node3");
		node3->setScale(0.1f, 0.1f, 0.1f);
		node3->setPosition(0, 20, 0);
		node3->attachObject(LightEnt);


		Ogre::SceneNode *node2 = mSceneMgr->createSceneNode("Node2");
		mSceneMgr->getRootSceneNode()->addChild(node2);
		node2->setPosition(500, 20, 0);

		Ogre::Light *light2 = mSceneMgr->createLight("Light2");
		light2->setType(Ogre::Light::LT_SPOTLIGHT);
		light2->setDirection(Ogre::Vector3(1,-1,0));
		light2->setSpotlightInnerAngle(Ogre::Degree(5.0f));
		light2->setSpotlightOuterAngle(Ogre::Degree(45.0f));
		light2->setSpotlightFalloff(0.0f);
		//light2->setPosition(50,20,0);
		light2->setDiffuseColour(1.0f, 0.0f, 1.0f);
		node2->attachObject(light2);

		Ogre::Entity *LightEnt2 = mSceneMgr->createEntity("MyEntity2", "sphere.mesh");
		Ogre::SceneNode* node22 = node2->createChildSceneNode("node22");
		node22->setScale(0.1f, 0.1f, 0.1f);
		
		node22->attachObject(LightEnt2);

	}

};

class Example41:public ExampleApplication{
public:
	void createScene(){
		//std::cout<<mSceneMgr->getTypeName()<<"::"<<mSceneMgr->getName()<<std::endl;
	}

	virtual void chooseSceneManager(){
		ResourceGroupManager::getSingleton().addResourceLocation("C:/ogre/OgreSDK_vc10_v1-8-1/media/packs/chiropteraDM.pk3", "Zip",
			ResourceGroupManager::getSingleton().getWorldResourceGroupName(), true);
		ResourceGroupManager::getSingleton().initialiseResourceGroup(
			ResourceGroupManager::getSingleton().getWorldResourceGroupName());
		mSceneMgr = mRoot->createSceneManager("BspSceneManager");
		mSceneMgr->setWorldGeometry("maps/chiropteradm.bsp");
	}

};

class Example43:public ExampleApplication{
private:

public:
	void createScene(){
		Ogre::Plane plane(Vector3::UNIT_Y, -10);
		Ogre::MeshManager::getSingleton().createPlane("plane", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
			plane, 1500, 1500, 200, 200, true, 1, 5, 5, Vector3::UNIT_Z);
		Ogre::Entity *ent = mSceneMgr->createEntity("GrassPlane", "plane");
		mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(ent);
		ent->setMaterialName("Examples/GrassFloor");
		Ogre::Light *light = mSceneMgr->createLight("Light1");
		light->setType(Ogre::Light::LT_DIRECTIONAL);
		light->setDirection(Ogre::Vector3(1,-1,0));
		Ogre::ManualObject *manual = mSceneMgr->createManualObject("grass");
		manual->begin("Examples/GrassBlades", RenderOperation::OT_TRIANGLE_LIST);

		manual->position(5.0, 0.0, 0.0);
		manual->textureCoord(1,1);
		manual->position(-5.0, 10.0, 0.0);
		manual->textureCoord(0,0);
		manual->position(-5.0, 0.0, 0.0);
		manual->textureCoord(0,1);

		manual->position(5.0, 0.0, 0.0);
		manual->textureCoord(1,1);
		manual->position(5.0, 10.0, 0.0);
		manual->textureCoord(1,0);
		manual->position(-5.0, 10.0, 0.0);
		manual->textureCoord(0,0);

		//////////////////////////////////////////////////////////////////////////
		manual->position(2.5, 0.0, 4.3);
		manual->textureCoord(1,1);
		manual->position(-2.5, 10.0, -4.3);
		manual->textureCoord(0,0);
		manual->position(-2.0, 0.0, -4.3);
		manual->textureCoord(0,1);

		manual->position(2.5, 0.0, 4.3);
		manual->textureCoord(1,1);
		manual->position(2.5, 10.0, 4.3);
		manual->textureCoord(1,0);
		manual->position(-2.5, 10.0, -4.3);
		manual->textureCoord(0,0);

		manual->end();

		manual->convertToMesh("BladesOfGrass");
		for (int i=0; i<50; i++){
			for (int j=0; j<50; j++){
				Ogre:Entity *ent = mSceneMgr->createEntity("BladesOfGrass");
				Ogre::SceneNode *node = mSceneMgr->getRootSceneNode()->createChildSceneNode(
					Ogre::Vector3(i*3,-10,j*3));
				node->attachObject(ent);
			}
		}

		Ogre::SceneNode *grassNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("GrassNode2");
		grassNode->attachObject(manual);
	}
};

class Example44:public ExampleApplication{
private:

public:
	void createScene(){
		Ogre::Plane plane(Vector3::UNIT_Y, -10);
		Ogre::MeshManager::getSingleton().createPlane("plane", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
			plane, 1500, 1500, 200, 200, true, 1, 5, 5, Vector3::UNIT_Z);
		Ogre::Entity *ent = mSceneMgr->createEntity("GrassPlane", "plane");
		mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(ent);
		ent->setMaterialName("Examples/GrassFloor");
		Ogre::Light *light = mSceneMgr->createLight("Light1");
		light->setType(Ogre::Light::LT_DIRECTIONAL);
		light->setDirection(Ogre::Vector3(1,-1,0));
		Ogre::ManualObject *manual = mSceneMgr->createManualObject("grass");
		manual->begin("Examples/GrassBlades", RenderOperation::OT_TRIANGLE_LIST);
		
		manual->position(5.0, 0.0, 0.0);
		manual->textureCoord(1,1);
		manual->position(-5.0, 10.0, 0.0);
		manual->textureCoord(0,0);
		manual->position(-5.0, 0.0, 0.0);
		manual->textureCoord(0,1);
		manual->position(5.0, 10.0, 0.0);
		manual->textureCoord(1,0);

		manual->position(2.5, 0.0, 4.3);
		manual->textureCoord(1,1);
		manual->position(-2.5, 10.0, -4.3);
		manual->textureCoord(0,0);
		manual->position(-2.0, 0.0, -4.3);
		manual->textureCoord(0,1);
		manual->position(2.5, 10.0, 4.3);
		manual->textureCoord(1,0);
		

		manual->index(0);
		manual->index(1);
		manual->index(2);
		manual->index(0);
		manual->index(3);
		manual->index(1);
		manual->index(4);
		manual->index(5);
		manual->index(6);
		manual->index(4);
		manual->index(7);
		manual->index(5);
		manual->end();

		

		manual->convertToMesh("BladesOfGrass");

		Ogre::StaticGeometry *field = mSceneMgr->createStaticGeometry("FieldOfGrass");

		for (int i=0; i<50; i++){
			for (int j=0; j<50; j++){
				Ogre:Entity *ent = mSceneMgr->createEntity("BladesOfGrass");
				field->addEntity(ent, Ogre::Vector3(i*3,-10,j*3));
			}
		}
		field->build();
	}
};

int main(void){
	Example1 app;
	app.go();
	return 0;
}
